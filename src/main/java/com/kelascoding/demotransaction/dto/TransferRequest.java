package com.kelascoding.demotransaction.dto;

import lombok.Data;

@Data
public class TransferRequest {
    private String norekAsal;
    private String norekTujuan;
    private double nominal;  
}
