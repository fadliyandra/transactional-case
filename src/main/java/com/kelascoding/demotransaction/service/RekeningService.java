package com.kelascoding.demotransaction.service;

import org.springframework.stereotype.Service;

import com.kelascoding.demotransaction.entity.Rekening;
import com.kelascoding.demotransaction.repo.RekeningRepo;

import jakarta.transaction.Transactional;

@Service
public class RekeningService {
    
    private final RekeningRepo rekeningRepo;

    public RekeningService(RekeningRepo rekeningRepo) {
        this.rekeningRepo = rekeningRepo;
    }

    public Rekening createRekening(Rekening rekening){
         return rekeningRepo.save(rekening);
    }

    public Iterable<Rekening> getAllRekening(){
        return rekeningRepo.findAll();
    }

    @Transactional
    public void transfer(String norekAsal, String norekTujuan, Double nominal){
        Rekening asal = rekeningRepo.findByNorek(norekAsal);
        if (asal == null) {
            throw new RuntimeException("rekening asal tidak di temukan");
        }
        if (asal.getSaldo() <nominal) {
            throw new RuntimeException("saldo tidak mencukupi");
        }
        if (norekAsal.equals(norekTujuan)) {
            throw new RuntimeException("rekenig asal dan tujuan tidak boleh sama");    
        }
        if (nominal>1000) {
            throw new RuntimeException("Maksimal transfer 1000");
        }

        asal.setSaldo(asal.getSaldo() - nominal);
        rekeningRepo.save(asal);



        Rekening tujuan = rekeningRepo.findByNorek(norekTujuan);
        if (tujuan == null) {
            throw new RuntimeException("rekening tujuan tidak di temukan");
        }


        tujuan.setSaldo(tujuan.getSaldo() + nominal);
        rekeningRepo.save(tujuan);


    }


}
