package com.kelascoding.demotransaction.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kelascoding.demotransaction.dto.TransferRequest;
import com.kelascoding.demotransaction.entity.Rekening;
import com.kelascoding.demotransaction.service.RekeningService;

@RestController
@RequestMapping("/rekening")
public class RekeningController {
    
    private final RekeningService rekeningService;

    public RekeningController(RekeningService rekeningService) {
        this.rekeningService = rekeningService;
    }

        @PostMapping
        public Rekening creaRekening(@RequestBody Rekening rekening){
            return rekeningService.createRekening(rekening);
        }

        @GetMapping
        public Iterable <Rekening> getAllRekening(){
            return  rekeningService.getAllRekening();
        }

        @PostMapping("/transfer")
        public void transfer(@RequestBody TransferRequest request){
            rekeningService.transfer(request.getNorekAsal(), request.getNorekTujuan(), request.getNominal());
        }

}
