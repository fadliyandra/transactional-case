package com.kelascoding.demotransaction.repo;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kelascoding.demotransaction.entity.Rekening;

@Repository
public interface RekeningRepo extends CrudRepository<Rekening, Long>{

    Rekening findByNorek(String norek);
}
